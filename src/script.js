let mode = 'vaporwave';
let color = 'white';

getInputElement = () => document.getElementById('raw');

getGeneratedElement = () => document.getElementById('generated');

getElement = (id) => document.getElementById(id); 

setPlacholder = () => getInputElement().setAttribute('placeholder', mode === 'slack' ? 'Make some emoji ;)' : 'Make it  a e s t h e t i c !');

startup = () => {
    if (!!window.location.href.match(/slack/)) {
        selectSlack();
    }
    setPlacholder();
}

generate = () => {
    if (mode === 'vaporwave') {
        makeAesthetic();
    }

    if (mode === 'slack') {
        makeEmoji();
    }
}

switchMode = (newMode) => {
    mode = newMode;
    setPlacholder();
    const vaporwaveBtn = getElement('vaporwaveButton');
    const slackBtn = getElement('slackButton');
    const generated = getGeneratedElement()
    if (newMode === 'slack') {
        vaporwaveBtn.classList.add('grayed');
        slackBtn.classList.remove('grayed');
        generated.classList.remove('vaporwave');
        generated.classList.add('slack');
        document.getElementById('colors').setAttribute('style', 'display: flex');
    }
    if (newMode === 'vaporwave') {
        slackBtn.classList.add('grayed');
        vaporwaveBtn.classList.remove('grayed');
        generated.classList.remove('slack');
        generated.classList.add('vaporwave');
        document.getElementById('colors').setAttribute('style', 'display: none');
    }
}

selectVaporwave = () => {
    switchMode('vaporwave');
    generate();
}

selectSlack = () => {
    switchMode('slack');
    generate();
}

switchColor = (newColor) => {
    color = newColor;
    const whiteBtn = getElement('whiteButton');
    const yellowBtn = getElement('yellowButton');
    if (newColor === 'white') {
        whiteBtn.classList.remove('grayed');
        yellowBtn.classList.add('grayed');
    }
    if (newColor === 'yellow') {
        yellowBtn.classList.remove('grayed');
        whiteBtn.classList.add('grayed');
    }
}

selectWhite = () => {
    switchColor('white');
    generate();
}


selectYellow = () => {
    switchColor('yellow');
    generate();
}

setGenerated = (text, timeout) => {
    document.getElementById('copyButton').setAttribute('style', !text ? 'display: none' : 'display: block');
    setTimeout(() => {
        document.getElementById('generated').innerHTML = text;
    }, timeout);
}

copyText = () => {
    const el = document.createElement('textarea');
    el.value = getGeneratedElement().innerText;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

makeEmoji = () => {
    let text = getInputElement().value;
    text = text
        .replaceAll(/(\w)/g, `:alphabet-${color}-$1:`)
        .replaceAll(/(#)/g, `:alphabet-${color}-hash:`)
        .replaceAll(/(\?)/g, `:alphabet-${color}-question:`)
        .replaceAll(/(!)/g, `:alphabet-${color}-exclamation:`)
        .replaceAll(/(@)/g, `:alphabet-${color}-at:`)
        .replaceAll(' ', '&nbsp;&nbsp;&nbsp;');
    setGenerated(text, 50)
}

makeAesthetic = () => {
    let text = getInputElement().value;
    text = text.replaceAll(/(.)/g, '$1&nbsp;').replaceAll(' ', '&nbsp;&nbsp;');
    setGenerated(text, 200)
}
